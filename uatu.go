// Creado por: Javier Garcia, año 2021
package uatu // https://es.wikipedia.org/wiki/Uatu_(Marvel_Comics)

import (
	"gitlab.com/ariatel_ats/tools/logger"
	r "gopkg.in/rethinkdb/rethinkdb-go.v6"
)

var log logger.Logger = logger.Logger{Path: "dispatcher/uatu_connections.log"}

func init() {
	log.Start(7)
}

func StartConnection(address, port, database string) (session *r.Session, err error) {
	session, err = r.Connect(r.ConnectOpts{ // Nos conectamos a rethinkDB
		Address: address + ":" + port,
		Database: database,
	})
	if err != nil {
		return nil, err
	}
	return session, nil
}

func WatchChanges(table string, session *r.Session, updateFunc func(interface{}, interface{}, *r.Session), createFunc, deleteFunc func(interface{}, *r.Session)) {
	changes, err := r.Table(table).Changes().Run(session)
	var current_change interface{}
	if err != nil {
		log.Error.Fatalln("Ocurrió un error al observar cambios")
		return
	}

	for changes.Next(&current_change) {
		if current_change != nil {

			new_val, old_val := getValuesFromChange(current_change)
			action := getAction(new_val, old_val)
			
			switch action {
			case "update":
				updateFunc(new_val, old_val, session)
			case "create":
				createFunc(new_val, session)
			case "delete":
				deleteFunc(old_val, session)
			case "error":
			default:
				log.Warning.Println("Ocurrió un error en el switch de los cambios")
				session.Close()
				return
			}
		}
	}
}

func WatchChangesWithoutActions(table string, session *r.Session, dothis func(interface{}, interface{}, *r.Session)) {
	changes, err := r.Table(table).Changes().Run(session)
	var current_change interface{}
	if err != nil {
		log.Error.Fatalln("Ocurrió un error al mirar cambios sin acciones")
		return
	}

	for changes.Next(&current_change) {
		if current_change != nil {
			new_val, old_val := getValuesFromChange(current_change)
			dothis(new_val, old_val, session)
		}
	}
}

func getValuesFromChange(v interface{}) (new_val, old_val interface{}) {
	change, ok := v.(map[string]interface{})
	if !ok {
		return nil, nil
	} else {
		new_val = change["new_val"]
		old_val = change["old_val"]
	}
	return
}

func getAction(new_val interface{}, old_val interface{}) (action string) {
	new_value := (new_val != nil)
	old_value := (old_val != nil)
	// --- Solo un valor nuevo -> Insert
	// --- Solo un valor viejo -> Delete
	// --- Valor en ambos campos -> Update

	switch {
	case new_value && !old_value: // Insert
		return "create"
	case !new_value && old_value: // Delete
		return "delete"
	case new_value && old_value: // Update
		return "update"
	default:
		return "error"
	}
}
