module gitlab.com/ariatel_ats/tools/rethinkdb_event_handler

go 1.17

require (
	gitlab.com/ariatel_ats/tools/logger v0.0.0-20211015164508-d84397582262
	gopkg.in/rethinkdb/rethinkdb-go.v6 v6.2.1
)

require (
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/hailocab/go-hostpool v0.0.0-20160125115350-e80d13ce29ed // indirect
	github.com/opentracing/opentracing-go v1.2.0 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/net v0.0.0-20211014222326-fd004c51d1d6 // indirect
	golang.org/x/sys v0.0.0-20211013075003-97ac67df715c // indirect
	google.golang.org/protobuf v1.27.1 // indirect
	gopkg.in/cenkalti/backoff.v2 v2.2.1 // indirect
)
