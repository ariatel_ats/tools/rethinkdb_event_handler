package uatu

import (
	"testing"
)

func TestStartConnection(t *testing.T) {
	casos := []struct {
		address  string
		port     string
		database string
	}{
		{ // Conexion Correcta
			address:  "170.78.56.86",
			port:     "28015",
			database: "ac_dispatcher",
		},

		{ // Conexion Incorrecta
			address:  "170.78.56.86",
			port:     "9999",
			database: "ac_dispatcher",
		},
	}
	for _, caso := range casos {
		session, err := StartConnection(caso.address, caso.port, caso.database)

		if err != nil && caso.port == "28015" {
			t.Errorf("Error, no se esta conectando a la DB, got %v", err)
		}

		if err == nil && caso.port == "9999" {
			t.Errorf("Error, no da error cuando deberia")
		}

		if session != nil {
			session.Close()
		}
	}
}

func TestGetValuesFromChange(t *testing.T) {
	var v interface{}
	v = map[string]interface{}{
		"new_val": map[string]interface{}{"a": 1, "b": "hola"},
		"old_val": map[string]interface{}{"a": 1, "b": "hola"},
	}

	casos := []struct {
		value        interface{}
		expected_new interface{}
		expected_old interface{}
	}{
		{ // Todo correcto
			value:        v,
			expected_new: map[string]interface{}{"a": 1, "b": "hola"},
			expected_old: map[string]interface{}{"a": 1, "b": "hola"},
		},
		{ // Todo incorrecto
			value:        1,
			expected_new: nil,
			expected_old: nil,
		},
	}

	for _, caso := range casos {
		new, old := GetValuesFromChange(caso.value)

		if caso.expected_new == nil && new != nil {
			t.Errorf("Tipo de dato incorrecto en new, got %v expected %v", new, caso.expected_new)
		}
		if caso.expected_old == nil && old != nil {
			t.Errorf("Tipo de dato incorrecto en old, got %v expected %v", old, caso.expected_old)
		}
	}
}

func TestGetAction(t *testing.T) {
	casos := []struct {
		new      interface{}
		old      interface{}
		expected string
	}{
		{
			new:      nil,
			old:      nil,
			expected: "error",
		}, // error
		{
			new:      1,
			old:      nil,
			expected: "create",
		}, // create
		{
			new:      nil,
			old:      1,
			expected: "delete",
		}, // delete
		{
			new:      1,
			old:      1,
			expected: "update",
		}, // update
	}

	for _, caso := range casos {
		action := GetAction(caso.new, caso.old)
		if action != caso.expected {
			t.Errorf("Accion incorrecta, got %v expected %v", action, caso.expected)
		}
	}
}
